FROM voidlinux/voidlinux
LABEL maintainer="Jan Christian Grünhage <jan.christian@gruenhage.xyz>"

RUN xbps-install -Sy plantuml font-fira-ttf font-firacode bash

ENTRYPOINT "/usr/bin/bash"
